import re
from redmine import Redmine
from settings import (
    REDMINE_SERVER,
    KEY,
    PROJECT_IDENTIFIER,
    PROJECT_ID,
    WIKI_PAGE_TITLE,
    HLR_TRACKER_ID,
    STP_TRACKER_ID,
    LINK_TEMPLATE,
    PLUGIN_URL,
    WARNING_MESSAGE
)

main_issues = None
main_issues_unused_ids = None
related_issues = None
related_issues_unused_ids = None
redmine = None

warnings = []
errors = []


class AnError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def connect_to_redmine():
    print 'Authenticating to Redmine server...'
    return Redmine(REDMINE_SERVER, key=KEY)


def get_main_issues():
    issues = redmine.issue.filter(project_id=PROJECT_ID,
                                  tracker_id=STP_TRACKER_ID)
    main_issues = [issue for issue in issues if 'STP' in issue.subject]

    return main_issues


def get_related_issues():
    issues = redmine.issue.filter(project_id=PROJECT_ID,
                                  tracker_id=HLR_TRACKER_ID)
    related_issues = [issue for issue in issues if 'HLR' in issue.subject]

    return related_issues


def get_related_issue_ids(tag):
        related_issue_ids = [issue.id for issue in related_issues
                             if tag in issue.subject]

        return related_issue_ids


def remove_from_main_issues(issue_identifier):
    global main_issues_unused_ids
    while issue_identifier in main_issues_unused_ids:
        main_issues_unused_ids.remove(issue_identifier)


def remove_from_related_issues(issue_identifier):
    global related_issues_unused_ids
    while issue_identifier in related_issues_unused_ids:
        related_issues_unused_ids.remove(issue_identifier)


def check_for_unused_issues():
    global warnings
    for unused_id in main_issues_unused_ids:
        warning_template = 'Unused STP on ticket #{0}. See {1}/issues/{0}.'
        warning = warning_template.format(unused_id, REDMINE_SERVER)
        warnings.append(warning)


def display_statistics():
    global main_issues
    global main_issues_unused_ids
    used_ids = len(related_issues) - len(related_issues_unused_ids)
    coverage = float(used_ids) / float(len(related_issues))
    print
    print 'HLRs related to STPs:\t{}'.format(used_ids)
    print 'Total HLR issues:\t{}'.format(len(related_issues))
    print 'Coverage:\t\t{0:.1f}%'.format(coverage * 100)


def display_warnings():
    if warnings != []:
        print
        print 'Warnings:'

        for warning in warnings:
            print '  {}'.format(warning)


def display_errors():
    if errors != []:
        print
        print 'Errors:'

        for error in errors:
            print '  {}'.format(error)


class MainIssue:
    def __init__(self, tags):
        tags = tags.upper()
        tag_pattern = re.compile('<(STP-(?:.)+?)>')
        self.identifier = tag_pattern.match(tags).group(1)
        tags = tags.replace(self.identifier, '').replace('<>', '')
        self.related_issues = []
        self.related_issue_ids = []
        self.get_related_issues(tags)
        self.description = ''
        self.id = self.get_issue_id()
        self.header = ''

    def get_issue_id(self):
        main_issue_ids = [issue.id for issue in main_issues
                          if self.identifier in issue.subject]
        if len(main_issue_ids) == 0:
            return None
        elif len(main_issue_ids) == 1:
            return main_issue_ids[0]
        else:
            raise AnError('We have duplicate issues with the same STP tag')

    def get_related_issues(self, tags):
        related_tag_pattern = re.compile('<<(HLR-(?:.)+?)>>')
        match = related_tag_pattern.match(tags)
        if match is not None:
            tag = match.group(1)
            if get_related_issue_ids(tag) == []:
                error_template = '{} is related to a non-existing "{}".'
                error = error_template.format(self.identifier, tag)
                print '  {}'.format(error)
                errors.append(error)
            else:
                if tag in self.related_issues:
                    error_template = '{} is already related to "{}".'
                    error = error_template.format(self.identifier, tag)
                    print '  {}'.format(error)
                    errors.append(error)
                else:
                    for id in get_related_issue_ids(tag):
                        self.related_issue_ids.append(id)
                    self.related_issues.append(tag)
            tags = re.sub('<<HLR-(?:.)+?>>', '', tags, count=1)
            self.get_related_issues(tags)
        else:
            return

    @property
    def subject(self):
        return '[{}] {}'.format(self.identifier, self.header[4:].title())

    def save(self):
        self.description = self.description.encode('utf-8')
        if self.id is None:
            redmine_issue = redmine.issue.new()
            print 'Added new issue: {}...'.format(self.identifier)
            redmine_issue.description = "{}\n\n{}".format(self.description,
                                                          WARNING_MESSAGE)
        else:
            redmine_issue = redmine.issue.get(self.id)
            print 'Updated existing issue: {}...'.format(self.identifier)
            template = "{}\n\n{}\n\n{}"
            link = LINK_TEMPLATE.format(REDMINE_SERVER,
                                        PLUGIN_URL,
                                        self.id)
            redmine_issue.description = template.format(link,
                                                        self.description,
                                                        WARNING_MESSAGE)
            remove_from_main_issues(self.id)

        redmine_issue.project_id = PROJECT_IDENTIFIER
        redmine_issue.subject = self.subject

        redmine_issue.tracker_id = STP_TRACKER_ID
        redmine_issue.save()

        if self.id is None:
            self.id = redmine_issue.id
            self.save()
        self.relations = redmine_issue.relations

    def __str__(self):
        return '{}: {}'.format(self.identifier, ', '.join(self.optional_tags))


def is_duplicate(issue, issues):
    count = sum(1 for each_issue in issues
                if each_issue.identifier == issue.identifier)
    return count > 1


def line_contains_tags(line_text):
    line_text = line_text.upper()
    tags_pattern = re.compile('<(STP-(?:.)+)>')
    if tags_pattern.search(line_text) is not None:
        return True


def line_contains_headers(line_text):
    header_pattern = re.compile('(^h[1-4]{1})')
    if header_pattern.search(line_text) is not None:
        return True


def save_relation(issue_id, issue_to_id):
    try:
        redmine.issue_relation.create(issue_id=issue_id,
                                      issue_to_id=issue_to_id,
                                      relation_type='relates')
    except:
        pass


def main():
    global redmine
    global main_issues
    global main_issues_unused_ids
    global related_issues
    global related_issues_unused_ids
    redmine = connect_to_redmine()
    main_issues = get_main_issues()
    main_issues_unused_ids = [issue.id for issue in main_issues]
    related_issues = get_related_issues()
    related_issues_unused_ids = [issue.id for issue in related_issues]
    page = redmine.wiki_page.get(WIKI_PAGE_TITLE,
                                 project_id=PROJECT_IDENTIFIER)
    lines = page.text.split('\n')
    issues = []
    issue = None

    for line in lines:
        if line_contains_tags(line):
            if issue is not None:
                issues.append(issue)
            issue = MainIssue(line)
        elif line != '' and issue is not None:
            if line_contains_headers(line):
                issue.header = line
            else:
                issue.description = '\n'.join([issue.description, line])
                issue.description = issue.description.strip()
    else:
        if issue is not None:
            issues.append(issue)

    for issue in issues:
        if is_duplicate(issue, issues):
            error = 'Duplicate Main Issue "{}".'.format(issue.identifier)
            print 'Error: {}'.format(error)
            if error not in errors:
                errors.append(error)
        else:
            issue.save()

            for related_issue in issue.relations:
                redmine.issue_relation.delete(related_issue.id)

            for index, id in enumerate(issue.related_issue_ids):
                save_relation(issue.id, id)
                remove_from_related_issues(id)
                related_issue_tag = issue.related_issues[index]
                print '  Saved related issue: {}'.format(related_issue_tag)


if __name__ == "__main__":
    main()
    check_for_unused_issues()
    display_warnings()
    display_errors()
    display_statistics()
