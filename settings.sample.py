"""
Instructions:

Modify the following constants according to the environment this script
is running in.

Then, rename this file from settings.sample.py to settings.py.
"""

REDMINE_SERVER = 'http://192.168.1.66'
KEY = ''

PROJECT_IDENTIFIER = 'ec-mps-base-certif'
PROJECT_ID = 36
WIKI_PAGE_TITLE = 'STP'
HLR_TRACKER_ID = 5
STP_TRACKER_ID = 8

PLUGIN_URL = '/xb-workflow/str/?from_stp='

LINK_TEMPLATE = "p(box). Click to create STR for this STP: {}{}{}.\nWARNING: The generated STR will have the same target version as this STP."

WARNING_MESSAGE = 'p(conflict). WARNING: Do not update this issue subject and/or description. This issue was automatically created/updated by the following wiki page: {}/projects/{}/wiki/{}'.format(REDMINE_SERVER, PROJECT_IDENTIFIER, WIKI_PAGE_TITLE)
