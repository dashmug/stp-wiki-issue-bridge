Setup
=====

In the terminal, run the following...

    $ pip install python-redmine

Rename `settings.sample.py` to `settings.py`.

Then, edit the following constants in `settings.py` to configure it for
the environment it is in.

    REDMINE_SERVER = ''
    KEY = ''

    PROJECT_IDENTIFIER = ''
    PROJECT_ID = 0
    WIKI_PAGE_TITLE = ''
    HLR_TRACKER_ID = 0
    STP_TRACKER_ID = 0

    WARNING_MESSAGE = ''

Execution
=========

    $ python update-STP-issues.py
